package com.matm.matmsdk.ChooseCard;

import android.os.Build;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;

import androidx.appcompat.app.AppCompatActivity;

import isumatm.androidsdk.equitas.R;

public class ChooseCardActivity extends AppCompatActivity {

    public static ChooseCardActivity instance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_card);
        instance=this;
    }

    void doFinish(){
        instance.finish();
    }
}
